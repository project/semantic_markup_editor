RDFa WYSIWYG editor Drupal module

This module is intended to make annotating text documents with RDFa meta data easier via a WYSIWYG interface.

INSTALLATION:
====
As usual,
1.) unpack the tarball
2.) copy the unpacked rdfa_editor directory to your Drupal installation's sites/all/modules directory
3.) enable at the admin/build/modules page
4.) try to edit or create a new node

