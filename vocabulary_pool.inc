<?php

define('SME_CACHING_CRON',      0x01);
define('SME_CACHING_ONDEMAND',  0x02);
define('SME_CACHING_DEFAULT',   SME_CACHING_CRON|SME_CACHING_ONDEMAND);

define('SME_CACHE_TABLE', 'cache_semantic_markup_editor');

include_once 'vocabulary_pool.parsers.inc';

/**
 * Load a vocabulary from the database.
 *
 * @param int $svid
 * @param bool $reset
 * @return stdClass
 */
function semantic_vocabulary_load($svid, $reset = FALSE) {
  static $cache = array();
  if($reset) $cache = array();
  if(!isset($cache[$svid])) {
    $cache[$svid] = db_fetch_object(db_query('SELECT * FROM {semantic_vocabulary} WHERE svid = %d', $svid));
  }
  return $cache[$svid];
}

/**
 * Saves a vocabulary to the database.
 *
 * @param stdClass $vocabulary
 */
function semantic_vocabulary_save($vocabulary) {
  if(isset($vocabulary->svid))
    db_query('DELETE FROM {cache_semantic_markup_editor} WHERE cid = \'%d\'', $vocabulary->svid);
  return (isset($vocabulary->url)) ?
    drupal_write_record('semantic_vocabulary', $vocabulary, isset($vocabulary->svid) ? 'svid' : array()):
    FALSE;
}


function semantic_vocabulary_delete($svid) {
  db_query('DELETE FROM {cache_semantic_markup_editor} WHERE cid = \'%d\'', $svid);
  db_query('DELETE FROM {semantic_vocabulary} WHERE svid = %d', $svid);
  db_query('DELETE FROM {cache_semantic_markup_editor} WHERE cid = \'%d\'', $svid);
}


/**
 * Loads all vocabularies.
 *
 * @return array
 */
function semantic_vocabulary_load_all() {
  $ret = array();
  $res = db_query('SELECT * FROM {semantic_vocabulary} ORDER BY weight ASC');
  while($row = db_fetch_object($res)) {
    $ret[$row->name] = $row;
  }
  return $ret;
}

/**
 * Cron helper function for the vocabulary pool.
 */
function vocabulary_pool_cron() {
  if(!(variable_get('semantic_markup_editor_caching_strategy', SME_CACHING_DEFAULT) & SME_CACHING_CRON)) return;
  cache_clear_all('*', SME_CACHE_TABLE, TRUE);
  $res = db_query('SELECT * FROM {semantic_vocabulary} WHERE cache = 1');
  while($row = db_fetch_object($res)) {
    cache_set($row->svid, vocabulary_pool_get_remote_data($row), SME_CACHE_TABLE);
  }
}

/**
 * Page to print JSON data
 *
 * @return NULL
 */
function vocabulary_pool_page() {
  $args = func_get_args();
  if(count($args) == 0) {
    print drupal_to_js(vocabulary_pool_retrieve(semantic_vocabulary_load_all()));
  } else {
    $svid = array_shift($args);
    $v = semantic_vocabulary_load($svid);
    if(!is_object($v)) {
      drupal_not_found();
      return NULL;
    }
    $v->parameters = $args;
    print drupal_to_js(vocabulary_pool_retrieve($v));
  }
  return NULL;
}

/**
 * Retrieves multiple pool objects.
 *
 * @todo cleanup a bit :)
 *
 * @staticvar array $cache
 * @param array $objects
 * @param bool $reset
 * @return mixed
 */
function vocabulary_pool_retrieve($objects, $reset = FALSE) {
  static $cache = array();
  if($reset) $cache = array();
  if(!is_array($objects)) $objects = array($objects);
  $ret = array();
  foreach($objects as $o) {
    if(!isset($o->parameters)) $o->parameters = NULL;
    $cache_string = $o->svid . (count($o->parameters) ?
      '_'.implode('::', $o->parameters) : '');
    if(isset($cache[$o->svid])) {
      $ret[$o->svid] = $cache[$o->svid];
      continue;
    }
    if($o->cache) {
      $res = cache_get($cache_string, SME_CACHE_TABLE);
      if(is_object($res)) {
        $cache[$o->svid] = $ret[$o->svid] = $res->data;
        continue;
      } else {
        if(variable_get('semantic_markup_editor_caching_strategy', SME_CACHING_DEFAULT) & SME_CACHING_ONDEMAND) {
          $data = vocabulary_pool_get_remote_data($o);
          cache_set($cache_string, $data, SME_CACHE_TABLE);
          $cache[$o->svid] = $ret[$o->svid] = $data;
          continue;
        } else {
          $cache[$o->svid] = $ret[$o->svid] = NULL;
          continue;
        }
      }
    } else {
      $cache[$o->svid] = $ret[$o->svid] = vocabulary_pool_get_remote_data($o);
    }
  }

  return $ret;
  // TODO add a better merging alg instead of this:
  //return call_user_func_array('array_merge_recursive', $ret);
}

/**
 * Controls the data fetching.
 *
 * Firstly, it retrieves the data from a remote server.
 * Secondly, it parses it with the associated parser.
 *
 * @staticvar array $protocols
 * @param stdClass $vocabulary
 * @param array $parameters
 * @return mixed
 */
function vocabulary_pool_get_remote_data($vocabulary, $parameters = array()) {
  static $protocols = NULL;
  if($protocols === NULL) $protocols = vocabulary_pool_get_supported_protocols();
  if(count($parameters) == 0 && isset($vocabulary->parameters)) $parameters = $vocabulary->parameters;
  $data = FALSE;
  if(isset($protocols[$vocabulary->protocol]['callback']) && function_exists($protocols[$vocabulary->protocol]['callback'])) {
    $data = call_user_func($protocols[$vocabulary->protocol]['callback'], $vocabulary->url, $parameters, $vocabulary->username, $vocabulary->password);
  }
  if($data === FALSE) return NULL;
  return _vocabulary_pool_invoke_parser($vocabulary->format, $data);
}

/**
 * Retrieves content from a remote server via HTTP/HTTPS.
 *
 * @todo support for authentication
 *
 * @param string $url
 * @param array $parameters
 * @param string $username
 * @param string $password
 * @return string
 */
function vocabulary_pool_http_download($url, $parameters, $username = NULL, $password = NULL) {
  // security parameters; with these, the sprintf will always get enough parameters
  $sp = array_fill(0, max(1, substr_count($url, '%s')), NULL);
  // obj: request, data, headers, code
  $res = drupal_http_request(call_user_func_array('sprintf', array_merge(array($url), $parameters, $sp)));
  return ((int)$res->code{0} == 2) ? $res->data : FALSE;
}

/**
 * Dynamically invokes a parser.
 *
 * @staticvar array $parsers
 * @param string $parser
 * @param mixed $data
 * @return mixed
 */
function _vocabulary_pool_invoke_parser($parser, $data) {
  static $parsers = NULL;

  $args = func_get_args(); // this line is completely unnecessary... oh, wait, PHPWTF.

  if($parsers === NULL)
    $parsers = vocabulary_pool_get_supported_formats();

  if(!isset($parsers[$parser])) return NULL;

  return call_user_func_array($parsers[$parser]['callback'], array_slice($args, 1));
}

/**
 * Returns the lsit of the supported protocols.
 *
 * @return array
 */
function vocabulary_pool_get_supported_protocols() {
  return module_invoke_all('vocabulary_pool_protocol');
}

/**
 * Implementation of hook_vocabulary_pool_protocol().
 *
 * @return array
 */
function semantic_markup_editor_vocabulary_pool_protocol() {
  return array(
    'http' => array(
      'name' => t('HTTP/HTTPS'),
      'callback' => 'vocabulary_pool_http_download',
      'URL description' => t('Place %s as a parameter placeholder in the URL.'),
    ),
  );
}

/**
 * Returns the list of the supported formats.
 *
 * @return array
 */
function vocabulary_pool_get_supported_formats() {
  return module_invoke_all('vocabulary_pool_parser');
}

/**
 * Implementation of hook_vocabulary_pool_parser().
 *
 * @return array
 */
function semantic_markup_editor_vocabulary_pool_parser() {
  return array(
    '' => array(
      'name' => t('Autodetect'),
      'callback' => 'vocabulary_pool_guess_format',
    ),
    'json' => array(
      'name' => 'JSON',
      'callback' => 'vocabulary_pool_parse_json',
    ),
    'rdf' => array(
      'name' => 'RDF',
      'callback' => 'vocabulary_pool_parse_rdf',
    ),
  );
}