<?php

/**
 * Implementation of hook_wysiwyg_include_directory().
 *
 * @param string $type
 * @return string
 */
function semantic_markup_editor_wysiwyg_include_directory($type) {
  switch ($type) {
    case 'editors':
      return 'plugins';
  }
}