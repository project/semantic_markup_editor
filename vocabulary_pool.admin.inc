<?php

include_once 'vocabulary_pool.inc';

function vocabulary_pool_admin_page_list() {
  // TODO add support for weight drag and drop (like block module)
  $rows = array();
  $res = db_query('SELECT * FROM {semantic_vocabulary} ORDER BY weight DESC');
  $formats = vocabulary_pool_get_supported_formats();
  $protocols = vocabulary_pool_get_supported_protocols();
  while($row = db_fetch_array($res)) {
    $rows []= array(
      check_plain($row['name']),
      $row['url'],
      $formats[$row['format']]['name'],
      $protocols[$row['protocol']]['name'],
      l(t('Edit'), 'admin/settings/semantic_markup_editor/vocabularies/edit/'.$row['svid']),
      l(t('Delete'), 'admin/settings/semantic_markup_editor/vocabularies/delete/'.$row['svid']),
    );
  }
  return theme('table', array(t('Name'), t('Vocabulary URL'), t('Format'), t('Protocol'), '&nbsp;', '&nbsp;'), $rows);
}

function vocabulary_pool_admin_cache_form($form) {
  $f = array();

  $f['semantic_markup_editor_caching_strategy'] = array(
    '#type' => 'select',
    '#options' => array(
      SME_CACHING_CRON => t('Cron only'),
      SME_CACHING_ONDEMAND => t('Transparent caching only'),
      SME_CACHING_CRON|SME_CACHING_ONDEMAND => t('Both'),
    ),
    '#title' => t('Caching strategy'),
    '#default_value' => variable_get('semantic_markup_editor_caching_strategy', SME_CACHING_DEFAULT),
  );

  return system_settings_form($f);
}

function vocabulary_pool_add_form($form, $values = array()) {
  $f = array();

  $description = t('Notes:').'<ul>';
  foreach(vocabulary_pool_get_supported_protocols() as $p)
    if(isset($p['URL description']))
      $description .= "<li><strong>{$p['name']}:</strong> {$p['URL description']}</li>";
  $description .= '</ul>';

  $f['name'] = array(
    '#type' => 'textfield',
    '#title' => t('Machine readable name'),
    '#description' => t('Allowed letters: a-z0-9_ It must be unique.'),
    '#required' => TRUE,
    '#default_value' => isset($values['name']) ? $values['name'] : '',
  );

  $f['url'] = array(
    '#type' => 'textfield',
    '#title' => t('URL'),
    '#required' => TRUE,
    '#default_value' => isset($values['url']) ? $values['url'] : '',
    '#description' => $description,
  );

  $f['cache'] = array(
    '#type' => 'checkbox',
    '#title' => t('Enable cache'),
    '#default_value' => isset($values['cache']) ? $values['cache'] : TRUE,
  );

  $options = array();
  foreach(vocabulary_pool_get_supported_formats() as $k=>$v)
    $options[$k] = $v['name'];

  $f['format'] = array(
    '#type' => 'select',
    '#options' => $options,
    '#title' => t('Format'),
    '#default_value' => isset($values['format']) ? $values['format'] : '',
  );

  $options = array();
  foreach(vocabulary_pool_get_supported_protocols() as $k=>$v)
    $options[$k] = $v['name'];

  $f['protocol'] = array(
    '#type' => 'select',
    '#options' => $options,
    '#title' => t('Protocol'),
    '#default_value' => isset($values['protocol']) ? $values['protocol'] : '',
  );

  $f['authentication'] = array(
    '#type' => 'fieldset',
    '#title' => t('Authentication'),
    '#collapsible' => TRUE,
    '#collapsed' => empty($values['username']) && empty($values['password']),
  );

  $f['authentication']['username'] = array(
    '#type' => 'textfield',
    '#title' => t('Username'),
    '#default_value' => isset($values['username']) ? $values['username'] : '',
  );

  $f['authentication']['password'] = array(
    '#type' => 'password',
    '#title' => t('Password'),
  );

  $f['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
  );

  return $f;
}


function vocabulary_pool_add_form_validate($form, &$form_state) {
  if (!preg_match('/^[a-z0-9_]+$/i', $form_state['values']['name'])) {
    form_set_error('name', t('Name field allows only: a-z0-9_'));
  }

  $result = db_fetch_object(db_query('SELECT COUNT(*) count FROM {semantic_vocabulary} WHERE name = \'%s\';', $form_state['values']['name']));
  if ($result->count >= 1) {
    form_set_error('name', t('Name field must be unique.'));
  }
}


function vocabulary_pool_add_form_submit($form, &$form_state) {
  $vocabulary = new stdClass();
  $vocabulary->name = check_plain($form_state['values']['name']);
  $vocabulary->url = $form_state['values']['url'];
  $vocabulary->cache = (int)$form_state['values']['cache'];
  $vocabulary->format = $form_state['values']['format'];
  $vocabulary->protocol = $form_state['values']['protocol'];
  if($form_state['values']['username'] != '' || $form_state['values']['password'] != '') {
    $vocabulary->username = $form_state['values']['username'];
    $vocabulary->password = $form_state['values']['password'];
  }
  semantic_vocabulary_save($vocabulary);
  drupal_set_message(t('Vocabulary added.'));
  drupal_goto('admin/settings/semantic_markup_editor/vocabularies');
}

function vocabulary_pool_edit_form($form, $svid) {
  $v = semantic_vocabulary_load($svid);
  if($v === FALSE) {
    drupal_goto('admin/settings/semantic_markup_editor/vocabularies/add');
    return array();
  }
  $f = vocabulary_pool_add_form($form, (array)$v);
  $f['svid'] = array(
    '#type' => 'value',
    '#value' => $svid,
  );
  return $f;
}


function vocabulary_pool_edit_form_validate($form, &$form_state) {
  // @TODO duplicated with: vocabulary_pool_add_form_validate, sorry
  if (!preg_match('/^[a-z0-9_]+$/i', $form_state['values']['name'])) {
    form_set_error('name', t('Name field allows only: a-z0-9_'));
  }

  // @TODO proper name-uniqueness validation.
}


function vocabulary_pool_edit_form_submit($form, &$form_state) {
  $vocabulary = new stdClass();
  $vocabulary->name = check_plain($form_state['values']['name']);
  $vocabulary->url = $form_state['values']['url'];
  $vocabulary->cache = (int)$form_state['values']['cache'];
  $vocabulary->format = $form_state['values']['format'];
  $vocabulary->svid = (int)$form_state['values']['svid'];
  $vocabulary->protocol = $form_state['values']['protocol'];
  $vocabulary->username = $form_state['values']['username'] != '' ?
    $form_state['values']['username']:
    NULL;
  if($form_state['values']['password'] != '') {
    $vocabulary->password = $form_state['values']['password'];
  }
  semantic_vocabulary_save($vocabulary);
  drupal_set_message(t('Vocabulary settings updated.'));
  drupal_goto('admin/settings/semantic_markup_editor/vocabularies');
}

function vocabulary_pool_delete_form($form, $svid) {
  $f = array();

  drupal_set_title('Are you sure that you want to delete this vocabulary?');

  $f['svid'] = array(
    '#type' => 'value',
    '#value' => $svid,
  );

  $f['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Delete'),
  );

  return $f;
}

function vocabulary_pool_delete_form_submit($form, &$form_state) {
  $svid = $form_state['values']['svid'];
  db_query('DELETE FROM {semantic_vocabulary} WHERE svid = %d', $svid);
  db_query('DELETE FROM {cache_semantic_markup_editor} WHERE cid = \'%d\'', $svid);
  drupal_set_message(t('Vocabulary deleted.'));
  drupal_goto('admin/settings/semantic_markup_editor/vocabularies');
}