

Drupal.SemanticMarkupEditor     = Drupal.SemanticMarkupEditor     || {};
Drupal.SemanticMarkupEditor.API = Drupal.SemanticMarkupEditor.API || {};


/**
 * Get selected text.
 */
Drupal.SemanticMarkupEditor.API.getSelectedContent = function() {
  var selection = Drupal.SemanticMarkupEditor.API.getSelection();
  if (selection.createRange) {
    return selection.createRange();
  } else {
    return selection.getRangeAt(0);
  }
}


Drupal.SemanticMarkupEditor.API.getSelection = function() {
  var selection;

  var iframe = $.fn.SemanticMarkupEditor.iframe();

  if (iframe.document.selection) {
    // IE selections
    selection = iframe.document.selection;
  } else {
    // Mozilla selections
    try {
      selection = iframe.getSelection();
    } catch(e) {
//      console.log(e);
    }
  }
  return selection;
}


function debug(o) {
  var a = [];
  for (var key in o) a.push(key);
  alert(a.join("\n"));
}


/**
 * Get the container node of the selected text.
 * @return false || Object
 */
Drupal.SemanticMarkupEditor.API.getSelectedNode = function() {
  var selection = Drupal.SemanticMarkupEditor.API.getSelection();
  var node = (selection.createRange) ?
    selection.createRange().parentElement():
    selection.anchorNode.parentNode;
  
  if (node.nodeName.toString().length == 0 || node.nodeName.toLowerCase() == 'body') {
    return false;
  }
  
  return node;
}
