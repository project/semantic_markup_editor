/**
  * jQuery RDFa Editor plugin
  *
  * Copyright (c) 2009 Ernő Zsemlye
  * Based on Batiste Bieler's jQuery rich text editor plugin.
  * Distributed under the GPL Licenses.
  */


Drupal.SemanticMarkupEditor = Drupal.SemanticMarkupEditor || {};


/**
  * Define the RDFaEditor plugin
  */
(function($) {

  $.fn.SemanticMarkupEditor = function(options) {

    $.fn.SemanticMarkupEditor.html = function(iframe) {
        return iframe.contentWindow.document.getElementsByTagName("body")[0].innerHTML;
    };

    $.fn.SemanticMarkupEditor.defaults = {
        miscPath: "",
        css: [],
        height: 500,
        rte_buttons: ['h1', 'h2', 'h3', 'strong', 'i', 'u']
    };

    /**
     * Returns the wysiwyg iframe.
     */
    $.fn.SemanticMarkupEditor.iframe = function() {
      return document.getElementById('iframe_semantic_markup_editor').contentWindow;
    }

    /**
     * Create an wysiwyg editor button.
     */
    $.fn.SemanticMarkupEditor.getRTEButtons = function(buttons) {
      var output = '';
      $.each(buttons, function(i, val) {
        output = output + '<a href="javascript:void(0);" class="rdfa-rte-button" html="' + val + '" id="rdfa-rte-button-' + val + '">&nbsp</a>&nbsp;';
      });
      return output;
    }

    $.fn.SemanticMarkupEditor.markupSelection = function(propertyData) {
      var iframe = $.fn.SemanticMarkupEditor.iframe();
      var selected = Drupal.SemanticMarkupEditor.API.getSelectedContent();
      if (selected.collapsed) {
        return;
      }

      if (selected.surroundContents) {
        newNode = iframe.document.createElement('span');
        newNode.setAttribute('property', propertyData.namespace + ':' + propertyData.title);
        selected.surroundContents(newNode);
      } else {
        var span = '<span property="' + propertyData.namespace + ':' + propertyData.title + '">' + selected.htmlText + '</span>';
        selected.pasteHTML(span);
      }
    }

    // build main options before element iteration
    var settings = $.extend($.fn.SemanticMarkupEditor.defaults, options);

    // iterate and construct the RDFaEditors
    return this.each(function() {

      var textarea = $(this);
      var iframe;
      var elementId = 'iframe_semantic_markup_editor';

      // enable design mode
      function createDesignMode() {

          var content = textarea.val();
          isWysiwygEnabled = false;

          // Mozilla needs this to display caret
          if($.trim(content)=='') {
              content = '<br />';
          }

          // for compatibility reasons, need to be created this way
          iframe = document.createElement("iframe");
          iframe.frameBorder = 0;
          iframe.frameMargin = 0;
          iframe.framePadding = 0;
          iframe.height = settings.height + 'px';
          iframe.name = 'editor_frame';
          iframe.id = elementId;
          if (textarea.attr('class')) {
            iframe.className = textarea.attr('class');
          }
          if (textarea.attr('name')) {
            iframe.title = textarea.attr('name');
          }

          $('#edit-body-wrapper label').after('<div id="semantic_markup_editor">\
            <div id="semantic_markup_editor_left"></div>\
            <div id="semantic_markup_editor_right"><div id="semantic_markup_editor_markups"></div></div>\
            </div>');
          $('#semantic_markup_editor_left').append('<div id="rdfa-buttons"></div>');
          $('#semantic_markup_editor_left').append(iframe);
            
          var buttons_wysiwyg = getWysiwygButtons();
          $('#rdfa-buttons').append(buttons_wysiwyg);
          var buttons_normal  = getNormalButtons();
          $('#semantic_markup_editor').before(buttons_normal);
          $('#rdfa-buttons-normal').hide();

          bindBasicEvents();
          Drupal.SemanticMarkupEditor.hideTextarea();

          var css = "";
          if (typeof(settings.css) == 'object') {
            for (var idx in settings.css) {
              css = css + "<link type='text/css' rel='stylesheet' href='" + settings.css[idx] + "' />";
            }
          }

          var doc = "<html><head>"+css+"</head><body class='frameBody'>"+content+"</body></html>";
          tryEnableDesignMode(doc);
      }

      /**
       * Enable wysiwyg editor.
       */
      function enableDesignMode() {
       var content = textarea.val();

        // Mozilla needs this to display caret
        if($.trim(content)=='') {
            content = '<br />';
        }

        Drupal.SemanticMarkupEditor.hideTextarea();
        $(iframe).contents().find("body").html(content);
        $('#semantic_markup_editor').show();
        $("#rdfa-buttons-wysiwyg").show();
        $("#rdfa-buttons-normal").hide();
        isWysiwygEnabled = true;

        return;
      }

      function bindBasicEvents() {
        // Update textarea's content on submit.
        $(iframe).parents('form').submit(function() {
          disableDesignMode(true);
        });

        $("#rdfa-close").click(function() {
          disableDesignMode();
          return false;
        });

        $("#rdfa-remove").click(function() {
          var removable_tags = settings.rte_buttons.concat([]);
          removable_tags.push('span');
          var node = Drupal.SemanticMarkupEditor.API.getSelectedNode();
          if (node !== false) {
            $(node).replaceWith($(node).html());
          }
        });

        $("#rdfa-edit-value").click(function(event) {
          var node = Drupal.SemanticMarkupEditor.API.getSelectedNode();
          if (node !== false && node.nodeName.toLowerCase() == 'span') {
            var default_value = $(node).attr('value');
            $('#semantic-markup-editor-value-tooltip-form #edit-value').val(default_value || '');
          }

          if (node === false) {
            event.stopPropagation();
            return false;
          }
          return true;
        });
        
        $('#semantic-markup-editor-value-tooltip-form').submit(function(){
          var node = Drupal.SemanticMarkupEditor.API.getSelectedNode();
          if (node !== false && node.nodeName.toLowerCase() == 'span') {
            var new_value = $('#semantic-markup-editor-value-tooltip-form #edit-value').val();
            if (new_value) {
              $(node).attr('value', new_value);
            }
          }
          tb_remove();
          return false;
        });

        $(".rdfa-rte-button").click(function() {
          var iframe = $.fn.SemanticMarkupEditor.iframe();
          var selected = Drupal.SemanticMarkupEditor.API.getSelectedContent();
          if (selected.collapsed) {
            return;
          }

          var span_class = $(this).attr('html');
          if (selected.surroundContents) {
            newNode = iframe.document.createElement('span');
            newNode.className = span_class;
            selected.surroundContents(newNode);
          } else {
            var span = '<span class="' + span_class + '">' + selected.htmlText + '</span>';
            selected.pasteHTML(span);
          }
        });

        $('#rdfa-enable').click(function(e){
          enableDesignMode();
          return false;
        });
      }

      function tryEnableDesignMode(doc) {
          if(!iframe) {
            return false;
          }

          try {
              iframe.contentWindow.document.open();
              iframe.contentWindow.document.write(doc);
              iframe.contentWindow.document.close();
          } catch(error) {}
          
          if (document.contentEditable) {
              return finishEnableDesignMode("On");
          } else if (document.designMode != null) {
              try {
                  return finishEnableDesignMode("on");
              } catch (error) {
                //console.log(error);
              }
          }
          setTimeout(function(){
            tryEnableDesignMode(doc);
          }, 500);
          return false;
      }

      function finishEnableDesignMode(on) {
        iframe.contentWindow.document.designMode = on;
        Drupal.SemanticMarkupEditor.Markup.loadMarkupBase();
        Drupal.SemanticMarkupEditor.Filters.refreshFilterings();
        isWysiwygEnabled = true;
        Drupal.attachBehaviors('#rdfa-main');
        return true;
      }

      function disableDesignMode(submit) {
          var content = $(iframe).contents().find("body").html();

          if(isWysiwygEnabled) {
              textarea.val(content);
          }

          if(submit != true) {
            Drupal.SemanticMarkupEditor.showTextarea();
            $('#semantic_markup_editor').hide();
          }
          $("#rdfa-buttons-wysiwyg").hide();
          $("#rdfa-buttons-normal").show();
          isWysiwygEnabled = false;
      }

      function getWysiwygButtons() {
        var buttons = '<div id="rdfa-buttons-wysiwyg"><div id="rdfa-main">';
        buttons += $.fn.SemanticMarkupEditor.getRTEButtons(settings.rte_buttons);
        buttons += '|';
        buttons += '<a id="rdfa-edit-value" title="Value" class="thickbox" href="#TB_inline?height=200&width=500&inlineId=semantic_markup_editor_value_panel">Value</a>';
        buttons += '|';
        buttons += '<a id="rdfa-remove" href="javascript:void(0);">Clean</a>';
        buttons += '|';
        buttons += '<a id="rdfa-close" href="javascript:void(0);">Text</a>';
        buttons += '</div></div>';
        return buttons;
      }

      function getNormalButtons() {
        return '<div id="rdfa-buttons-normal"><a id="rdfa-enable" href="javascript:void(0);">Enable RDFa editor</a></div>';
      }

      // enable design mode now
      createDesignMode();

    }); //return this.each
  };// RDFaEditor

})(jQuery);


Drupal.SemanticMarkupEditor.hideTextarea = function() {
  $('#edit-body').hide();
  $('.resizable-textarea').hide();
  $('.teaser-checkbox').hide();
}


Drupal.SemanticMarkupEditor.showTextarea = function() {
  $('#edit-body').show();
  $('.resizable-textarea').show();
  $('.teaser-checkbox').show();
}
