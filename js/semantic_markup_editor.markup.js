

Drupal.SemanticMarkupEditor = Drupal.SemanticMarkupEditor || {};
Drupal.SemanticMarkupEditor.Markup = Drupal.SemanticMarkupEditor.Markup || {};


Drupal.SemanticMarkupEditor.Markup.storage = {}
Drupal.SemanticMarkupEditor.Markup.id_prefix = 'sme_';
Drupal.SemanticMarkupEditor.Markup.id_fst = 1;


Drupal.SemanticMarkupEditor.Markup.loadMarkupBase = function() {
  $.ajax({
    type: 'POST',
    url: Drupal.settings.basePath + '?q=semantic_markup_editor/pool',
    data: '',
    dataType: 'json',
    success: function(response) {
      //console.log(response);
      // @TODO - attach items
      Drupal.SemanticMarkupEditor.Markup.AttachMarkups(response);
      Drupal.SemanticMarkupEditor.Markup.AttachEvents('#semantic_markup_editor_markups');
    },
    error: function(XMLHttpRequest, textStatus, errorThrown) {
//      console.log(textStatus);
//      console.log(errorThrown);
    }
  });
}


Drupal.SemanticMarkupEditor.Markup.AttachMarkups = function(markups) {
  var html = '';
  var zebra = true;
  for (var voc_id in markups) {
    html = html + '<ul>';
    for (var mid in markups[voc_id]) {
      //console.log(markups[voc_id][mid]);
      var unique_id = Drupal.SemanticMarkupEditor.Markup.generateUniqueID();
      Drupal.SemanticMarkupEditor.Markup.storage[unique_id] = markups[voc_id][mid];
      html = html + '<li class="markup ' + (zebra ? 'odd' : 'even')+ '"><a href="javascript:void(0);" id="' + unique_id + '">';
      html = html + markups[voc_id][mid].title;
      html = html + '</a></li>';
      zebra = !zebra;
    }
    html = html + '</ul>';
  }
  $('#semantic_markup_editor_markups').append(html);
}


Drupal.SemanticMarkupEditor.Markup.AttachEvents = function(context) {
  $('a', context).click(function(){
    var unique_id = $(this).attr('id');
    $.fn.SemanticMarkupEditor.markupSelection(Drupal.SemanticMarkupEditor.Markup.storage[unique_id]);
  });
}


Drupal.SemanticMarkupEditor.Markup.generateUniqueID = function() {
  return Drupal.SemanticMarkupEditor.Markup.id_prefix + Drupal.SemanticMarkupEditor.Markup.id_fst++;
}