

Drupal.SemanticMarkupEditor = Drupal.SemanticMarkupEditor || {};
Drupal.SemanticMarkupEditor.Filters = Drupal.SemanticMarkupEditor.Filters || {};

Drupal.SemanticMarkupEditor.Filters.refreshFilterings = function() {
//  console.log('Drupal.SemanticMarkupEditor.Filters.refreshFilterings');
  var iframe = $.fn.SemanticMarkupEditor.iframe();
  var htmlContent = $(iframe.document.body).html();

  for (idx in Drupal.settings.SemanticMarkupEditorFilter) {
    htmlContent = Drupal.SemanticMarkupEditor.Filters.filter(htmlContent, Drupal.settings.SemanticMarkupEditorFilter[idx]);
//    console.log('idx: ' + idx);
  }

  $(iframe.document.body).html(htmlContent);
}


Drupal.SemanticMarkupEditor.Filters.filter = function(htmlContent, filter) {
  var oContent = $('<div>' + htmlContent + '</div>');
  $('span', oContent).each(function(){
//    console.log(filter);
    var value = $(this).attr(filter.attribute);
    var regexp = new RegExp(filter.value, 'gi');
    if (regexp.test(value)) {
      $(this).addClass(filter['class']);
    }
  });
  return oContent.html();
}



