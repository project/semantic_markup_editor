/**
 * Colorizer - a helper class for giving visual feedback on annotated pieces of text.
 * Usage: Colorizer.getColor('class-name');
 * Randomly generates nice colors in a way that same class values get the same color.
 */

function Colorizer() {}
Colorizer.colors = {};
Colorizer.color_values = [];

Colorizer.generateAndSaveNewColor = function(name) {
  var i = 0;
  var color;
  do {
    color = Colorizer.generateNewColor();
  } while (i++ < 4096 && Colorizer.color_values.indexOf() >= 0);
  Colorizer.colors[name] = color;
  Colorizer.color_values.push(color);
};


Colorizer.generateNewColor = function() {
  var chars_dark   = [  8,  9,  'A', 'B'];
  var chars_middle = ['A', 'B', 'C', 'D'];
  var chars_light  = ['C', 'D', 'E', 'F'];
  var c_high, c_med, c_low;
  var colors = ['r', 'g', 'b'];
  var rnd;
  var ret;
  rnd = Math.floor(Math.random()*1000)%3;
  c_high = colors[rnd];
  colors.splice(rnd, 1);
  rnd = Math.floor(Math.random()*1000)%2;
  c_med = colors[rnd];
  colors.splice(rnd, 1);
  c_low = colors[0];
  ret = new Array();
  ret[c_high] = chars_light[Math.floor(Math.random() * 100000) % 4];
  ret[c_med]  = chars_middle[Math.floor(Math.random() * 100000) % 4];
  ret[c_low]  = chars_dark[Math.floor(Math.random() * 100000) % 4];
  return '' + ret.r + ret.g + ret.b;
}


Colorizer.getColor = function(name) {
 if (!Colorizer.colors.hasOwnProperty(name)) {
   Colorizer.generateAndSaveNewColor(name);
 }
 return Colorizer.colors[name];
};
