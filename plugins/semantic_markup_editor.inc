<?php

function semantic_markup_editor_semantic_markup_editor_editor() {
  $editor = array();
  $project_page = 'http://drupal.org/project/semantic_markup_editor';
  $editor['semantic_markup_editor'] = array(
    'title' => t('Semantic Markup Editor'),
    'vendor url' => $project_page,
    'download url' => $project_page,
    'library path' => drupal_get_path('module', 'semantic_markup_editor'),
    'libraries' => array(
      '' => array(
        'title' => t('Source'),
        'files' => array(
          'js/semantic_markup_editor.color.js',
          'js/jquery.SemanticMarkupEditor.js',
          'js/semantic_markup_editor.api.js',
          'js/semantic_markup_editor.markup.js',
          'js/semantic_markup_editor.util.js',
          'js/semantic_markup_editor.filters.js',
        ),
      )
    ),
    'version callback' => 'wysiwyg_sme_version',
    'load callback' => 'wysiwyg_sme_load',
    'settings callback' => 'wysiwyg_sme_settings',
    'versions' => array(
      '1.0' => array(
        'js files' => array('semantic_markup_editor.js')
      )
    ),
  );
  return $editor;
}

function wysiwyg_sme_load() {
  $path = drupal_get_path('module', 'semantic_markup_editor');
  drupal_add_css($path . '/css/semantic_markup_editor.css');
  drupal_add_css($path . '/css/semantic_markup_editor_example.css');
  drupal_add_css($path . '/css/semantic_markup_editor_wysiwyg.css');
  drupal_add_js(array('SemanticMarkupEditorPath' => $path), 'setting', 'footer');
}

function wysiwyg_sme_settings($editor, $config, $theme) {
  global $base_path;
  $path = drupal_get_path('module', 'semantic_markup_editor');
  return array('css' => array(
    $base_path.$path.'/css/semantic_markup_editor_wysiwyg.css',
  ));
}

function wysiwyg_sme_version($editor) {
  return '1.0';
}