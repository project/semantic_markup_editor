
Drupal.wysiwyg.editor.attach.semantic_markup_editor = function(context, params, settings) {
  $('#'+params.field).SemanticMarkupEditor(settings);
  // Trigger an event everytime SME editor getting ON.
  $('body').trigger('semanticMarkupEditor_attached', {});
}

Drupal.wysiwyg.editor.detach.semantic_markup_editor = function(context, params) {
  $("#rdfa-close").trigger('click');
  $("#rdfa-buttons-normal").remove();
}