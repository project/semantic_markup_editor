<?php


/**
 * Implementation of hook_features_export_options().
 *
 * @return array
 */
function vocabulary_pool_features_export_options() {
  module_load_include('inc', 'semantic_markup_editor', 'vocabulary_pool');
  $vocabularies = semantic_vocabulary_load_all();
  $voc_names = array();
  foreach ((array)$vocabularies as $name => $info) {
    $voc_names[$name] = $name;
  }

  return $voc_names;
}


/**
 * Implementation of hook_features_export().
 *
 * @param array $data
 * @param array $export
 * @param string $module_name
 * @return string
 */
function vocabulary_pool_features_export($data, &$export, $module_name = '') {
  foreach ((array)$data as $vocabulary) {
    $export['features']['vocabulary_pool'][$vocabulary] = $vocabulary;
  }

  $export['dependencies']['semantic_markup_editor'] = 'semantic_markup_editor';

  $pipes = array();
  return $pipes;
}


/**
 * Implementation of hook_features_export_render().
 *
 * @param string $module_name
 * @param array $data
 * @return array
 */
function vocabulary_pool_features_export_render($module_name = '', $data = array()) {
  module_load_include('inc', 'semantic_markup_editor', 'vocabulary_pool');
  $vocabularies = semantic_vocabulary_load_all();

  $code = array();
  $code[] = '  $data = array();';
  foreach ((array)$data as $vocabulary) {
    unset($vocabularies[$vocabulary]->svid);
    $code[] = '  $data[] = '.features_var_export((array)$vocabularies[$vocabulary], '  ').';';
  }
  $code[] = '  return $data;';

  return array(
    'vocabulary_pool_defaults' => join("\n", $code),
  );
}


/**
 * Implementation of hook_features_rebuild().
 */
function vocabulary_pool_features_rebuild() {
  module_load_include('inc', 'semantic_markup_editor', 'vocabulary_pool');
  $existing_vocabularies = semantic_vocabulary_load_all();
  
  $vocabularies_data = module_invoke_all('vocabulary_pool_defaults');
  foreach ((array)$vocabularies_data as $vocabulary_data) {
    if (!array_key_exists($vocabularies_data['name'], $existing_vocabularies)) {
      semantic_vocabulary_save((object)$vocabulary_data);
    }
  }
}


/**
 * Implementation of hook_features_revert().
 */
function vocabulary_pool_features_revert() {
  module_load_include('inc', 'semantic_markup_editor', 'vocabulary_pool');
  $existing_vocabularies = semantic_vocabulary_load_all();
  $vocabularies_data = module_invoke_all('vocabulary_pool_defaults');
  foreach ((array)$vocabularies_data as $vocabulary_data) {
    semantic_vocabulary_delete($existing_vocabularies[$vocabulary_data['name']]->svid);
  }

  vocabulary_pool_features_rebuild();
}
